//Luan Zelinski Corrêa - INFO 1 - Noturno
//
//Faça um programa em Java que receba o preço de custo
//de um produto e mostre o valor de venda. Sabe-se que o
//preço de custo receberá um acréscimo de acordo com um
//percentual informado pelo usuário.

import java.util.Scanner;
public class Exercicio11 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Digite o preço de custo: R$ ");
        float precocusto = input.nextFloat();
        System.out.println();

        System.out.print("Digite o valor do percentual de acréscimo: %");
        float percentual = input.nextFloat();
        System.out.println();

        System.out.println("Seus R$"+precocusto+" com %"+percentual+" de acréscimo resultarão em R$"+(precocusto*(1+(percentual/100))));
    }
}
