//Luan Zelinski Corrêa - INFO 1 - Noturno
//
//Faça um programa em Java que receba um número
//e diga se este número está no intervalo entre
//100 e 200.

import java.util.Scanner;
public class Exercicio15 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Digite um número qualquer: ");
        float numeroqualquer = input.nextFloat();
        System.out.println();

        if (numeroqualquer > 100 && numeroqualquer <200){
            System.out.println("Número entre 100 e 200");
        } else if (numeroqualquer <=100 || numeroqualquer >=200){
            System.out.println("Número menor ou igual a 100 ou maior ou igual a 200");
        }
    }
}
