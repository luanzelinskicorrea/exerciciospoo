//Luan Zelinski Corrêa - INFO 1 - Noturno
//
//Escrever um programa em Java que leia três
//valores inteiros distintos e os escreva em
//ordem crescente.

import java.util.Scanner;
import java.util.Arrays;

public class Exercicio28 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        float[] numerosacomparar = new float[3];

        for(int i=0; i<3; i++){
            System.out.print("Digite um número para compararmos: ");
            float numero = input.nextFloat();

            numerosacomparar[i] = numero;
        }

        System.out.println("");

        Arrays.sort(numerosacomparar);

        if(numerosacomparar[0] == numerosacomparar[1] ||
        numerosacomparar[0] == numerosacomparar[2]){
            System.out.println("Dessa vez você digitou alguns números iguais...");
            System.out.println("Mas relaxe... Deixaremos passar... Dessa vez!");
            System.out.println("");
        }

        System.out.println("Números em ordem crescente:");
        for(int j=0; j<3; j++){
            System.out.println(numerosacomparar[j]);
        }
    }
}
