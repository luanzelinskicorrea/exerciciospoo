//Luan Zelinski Corrêa - INFO 1 - Noturno
//
//Faça um programa em Java que receba o nome,
//a idade, o sexo e salário fixo de um funcionário.
//
//Mostre o nome e o salário líquido.
//
//Sexo M:
//  idade >= 30 --> abono de 100
//  idade  < 30 --> abono de 50
//
//Sexo F:
//  idade >= 30 --> 200
//  idade  < 30 --> 80

import java.util.Scanner;

public class Exercicio27 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Digite o nome do funcionário: ");
        String nome = input.next();

        System.out.print("Digite a idade do funcionário: ");
        int idade = input.nextInt();

        System.out.print("Digite o sexo do funcionário (M/F): ");
        String sexo = input.next();

        System.out.print("Digite o salário fixo do uncionário: R$ ");
        float salarioFixo = input.nextFloat();
        System.out.println("");

        if(sexo.equals("M")){
            if(idade >= 30){
                salarioFixo += 100;
            }else if(idade < 30){
                salarioFixo += 50;
            }
        }else if(sexo.equals("F")){
            if(idade >= 30){
                salarioFixo += 200;
            }else if(idade < 30){
                salarioFixo += 80;
            }
        }

        System.out.println("Nome do funcionário: " + nome);
        System.out.printf("Salário líquido: R$ %.2f", salarioFixo);
        System.out.println("");
    }
}
