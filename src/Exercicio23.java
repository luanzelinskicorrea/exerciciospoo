//Luan Zelinski Corrêa - INFO 1 - Noturno
//
//Faça um programa em Java que receba um número e mostre
//uma mensagem caso este número seja maior que 80, menor
//que 25 ou igual a 40.

import java.util.Scanner;

public class Exercicio23 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        System.out.print("Digite um número qualquer: ");
        float numeroqualquer = input.nextFloat();

        if(numeroqualquer > 80){
            System.out.println("Número maior que 80");
        }else if(numeroqualquer < 25){
            System.out.println("Número menor que 25");
        }else if(numeroqualquer == 40){
            System.out.println("Número igual a 40");
        }
    }
}
