//Luan Zelinski Corrêa - INFO 1 - Noturno
//
//Faça um programa em Java quereceba o número do
//mês e mostre o mês correspondente.

import java.util.Scanner;

public class Exercicio26 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String[] meses = {
            "Janeiro","Fevereiro","Março",
            "Abril","Maio","Junho",
            "Julho","Agosto","Setembro",
            "Outubro","Novembro","Dezembro" 
        };

        System.out.print("Digite um número correspondente a um mês: ");
        int numeromes = input.nextInt();

        if((numeromes - 1) < 0 || numeromes > meses.length){
            System.out.println("Mês inexistente");
        }else{
            System.out.println("Mês de " + meses[numeromes-1] + " - Mês " + numeromes);
        }
    }
}
