//Luan Zelinski Corrêa - INFO 1 - Noturno
//
//Escreer um programa em Java para determinar o consumo
//médio de um automóvel sendo fornecida a distância total
//percorrida pelo automóvel e o total de combustível gasto.

import java.util.Scanner;

public class Exercicio03 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Digite a distância total percorrida (em Km): ");
        float quilometros = input.nextFloat();
        System.out.println();

        System.out.print("Digite o total de combustível gasto (em L): ");
        float litros = input.nextFloat();
        System.out.println();

        System.out.println("O consumo médio foi de "+(quilometros/litros)+"Km/L");
    }
}