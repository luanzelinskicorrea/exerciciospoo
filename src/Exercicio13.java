//Luan Zelinski Corrêa - INFO 1 - Noturno
//
//Faça um programa em Java que receba um número e mostre
//uma mensagem caso este número seja maior que 10.

import java.util.Scanner;
public class Exercicio13 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);

        System.out.print("Digite um número inteiro: ");
        int numeroqualquer = input.nextInt();
        System.out.println();

        if(numeroqualquer > 10){
            System.out.println("Número MAIOR que 10");
        } else if (numeroqualquer == 10){
            System.out.println("Número IGUAL a 10");
        } else{
            System.out.println("Número MENOR que 10");
        }
    }
}
