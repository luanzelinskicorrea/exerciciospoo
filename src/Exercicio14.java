//Luan Zelinski Corrêa - INFO 1 - Noturno
//
//Escrever um programa em Java que leia dois
//valores inteiros distintos e informe qual
//é o maior número.

import java.util.Scanner;
public class Exercicio14 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Digite o primeiro número: ");
        Integer numero1 = input.nextInt();
        System.out.println();

        System.out.print("Digite o segundo número: ");
        Integer numero2 = input.nextInt();
        System.out.println();

        if(numero1 > numero2){
            System.out.println("Primeiro número maior que o segundo");
        } else if (numero1 < numero2){
            System.out.println("Segundo número maior que o primeiro");
        } else {
            System.out.println("Primeiro número igual ao segundo");
        }
    }
}
