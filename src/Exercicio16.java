//Luan Zelinski Corrêa - INFO 1 - Noturno
//
//Escrever um programa em Java que leia o nome e as três notas obtidas por um
//aluno durante o semestre. Calcular a sua média aritmética. Informar o nome e
//sua menção (REPROVADO, RECUPERAÇÃO ou APROVADO)
//
//REPROVADO - media <= 5
//RECUPERAÇÃO - media entre 5.1 e 6.9
//APROVADO - media >= 7

import java.util.Scanner;
public class Exercicio16 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Digite o nome do(a) aluno(a): ");
        String nome = input.next();
        System.out.println();

        System.out.print("Digite a primeira nota: ");
        float notaum = input.nextFloat();
        System.out.println();

        System.out.print("Digite a segunda nota: ");
        float notadois = input.nextFloat();
        System.out.println();

        System.out.print("Digite a terceira nota: ");
        float notatres = input.nextFloat();
        System.out.println();

        float media = ((notaum+notadois+notatres)/3);

        if (media <= 5){
            System.out.println("Aluno(a) reprovado(a)");
        } else if (media >= 5.1 && media <= 6.9){
            System.out.println("Aluno(a) em recuperação");
        }else{
            System.out.println("Aluno(a) aprovado(a)");
        }
    }
}
