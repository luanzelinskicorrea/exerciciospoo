//Luan Zelinski Corrêa - INFO 1 - Noturno
//
//A loja mamão com açúcar está vendendo seus
//produtos em 5 prestações sem juros. Faça um
//programa em Java que receba um valor de uma
//compra e mostre o valor das prestações.

import java.util.Scanner;
public class Exercicio10 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Digite o valor da compra: ");
        float valorcompra = input.nextFloat();
        System.out.println();

        for(int i=1; i<=5; i++){
            System.out.println(i+"ª prestação = R$ "+(valorcompra/5));
        }
    }
}
