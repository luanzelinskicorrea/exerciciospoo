//Luan Zelinski Corrêa - INFO 1 - Noturno
//
//Elaborar um programa em Java que efetue a apresentação
//do valor da conversão em real (R$) de um valor lido em
//dólar (US$). O programa em Java deverá solicitar o valor
//da cotação do dólar e também a quantidade de dólares
//disponíveis para o usuário.

import java.util.Scanner;
public class Exercicio08 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Digite o valor em dólar disponível: ");
        float dolar = input.nextFloat();
        System.out.println();

        System.out.print("Digite o valor da cotação do dólar: ");
        float cotacao = input.nextFloat();
        System.out.println();

        System.out.println(dolar+" dólares = "+(cotacao * dolar)+" reais");
    }
}
