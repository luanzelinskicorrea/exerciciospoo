//Luan Zelinski Corrêa - INFO 1 - Noturno
//
//Ler dois valores para as variáveis A e B, e efetuar as trocas
//dos valores de forma que a variável A passe a possuir o valor da
//variavel B e a variável B passe a possuir o valor da variável A.
//Apresentar os valores trocados.

import java.util.Scanner;
public class Exercicio06 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Valor da variável A: ");
        float variavelA = input.nextFloat();
        System.out.println();

        System.out.print("Valor da variavel B: ");
        float variavelB = input.nextFloat();
        System.out.println();

        float temp;

        temp = variavelA;
        variavelA = variavelB;
        variavelB = temp;

        System.out.println("Variável A: "+variavelA);
        System.out.println("Variável B: "+variavelB);

    }
}
