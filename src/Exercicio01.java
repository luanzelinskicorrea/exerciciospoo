//Luan Zelinski Corrêa - INFO 1 - Noturno
//
//Faça um programa em Java que receba dois
//números e exiba o resultado da sua soma.

import java.util.Scanner;
public class Exercicio01 {
    public static void main(String[] args){

        Scanner input = new Scanner(System.in);

        System.out.print("Digite o valor do primeiro número: ");
        float numero1 = input.nextFloat();
        System.out.println();

        System.out.print("Digite o valor do segundo número: ");
        float numero2 = input.nextFloat();
        System.out.println();

        System.out.println("O resultado da soma entre "+numero1+" e "+numero2+" é "+(numero1+numero2));
    }
}