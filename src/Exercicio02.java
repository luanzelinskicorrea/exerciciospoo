//Luan Zelinski Corrêa - INFO 1 - Noturno
//
//Faça um programa em Java que receba dois
//números e exiba as quatro operações básicas

import java.util.Scanner;
public class Exercicio02 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Digite o valor do primeiro número: ");
        float numero1 = input.nextFloat();
        System.out.println();

        System.out.print("Digite o valor do segundo número: ");
        float numero2 = input.nextFloat();
        System.out.println();

        System.out.println("O resultado da adição entre "+numero1+" e "+numero2+" é igual a: "+(numero1+numero2));
        System.out.println("O resultado da subtração entre "+numero1+" e "+numero2+" é igual a: "+(numero1-numero2));
        System.out.println("O resultado da multiplicação entre "+numero1+" e "+numero2+" é igual a: "+(numero1*numero2));
        System.out.println("O resultado da divisão entre "+numero1+" e "+numero2+" é igual a: "+(numero1/numero2));
    }
}
