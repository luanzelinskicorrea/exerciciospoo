//Luan Zelinski Corrêa - INFO 1 - Noturno
//
//Ler uma temperatura em graus Celsius e apresentá-la convertida
//em graus Fahrenheit. A fórmula de conversão é: F=(9*C+160)/5, sendo
//F a temperatura em Fahrenheit e C a temperaura em Celsius.

import java.util.Scanner;
public class Exercicio07 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Digite a temperatura em Celsius: ");
        float celsius = input.nextFloat();
        System.out.println();

        System.out.println("A temperatura em Fahrenheit é de: "+((9*celsius+160)/5));
    }
}
