//Luan Zelinski Corrêa - INFO 1 - Noturno
//
//Faça um programa em Java que receba um valor
//que foi depositado e exiba o valor com rendimento
//após um mês. Considere fixo o juro da poupança
//em 0,70% a.m.

import java.util.Scanner;

public class Exercicio09 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Digite o valor depositado na poupança: ");
        float deposito = input.nextFloat();
        System.out.println();

        System.out.print("Após um mês, seus R$"+deposito+" valerão R$ "+(deposito*1.007));
    }
}
