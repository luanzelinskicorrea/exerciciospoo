//Luan Zelinski Corrêa - INFO 1 - Noturno
//
//Escrever um programa que leia o nome de um vendedor,
//o seu salário fixo e o total de vendas efetuadas por
//ele no mês (em dinheiro). Sabendo que este vendedor
//ganha 15% de comissão sobre suas vendas efetuadas,
//informar o seu nome, salário fixo e salário no final
//do mês.

import java.util.Scanner;
public class Exercicio04{
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);

        System.out.print("Digite o nome do vendedor: ");
        String nome = input.next();
        System.out.println();

        System.out.print("Digite o valor do salário fixo: ");
        float salariofixo = input.nextFloat();
        System.out.println();

        System.out.print("Digite o valor total de vendas: ");
        float totalvendas = input.nextFloat();
        System.out.println();

        System.out.println("Nome do vendedor: "+nome);
        System.out.println("Salário fixo: "+salariofixo);
        System.out.println("Total de vendas: "+totalvendas);
        System.out.println("Salário bonificado: "+(salariofixo+(0.15*totalvendas)));
    }
}