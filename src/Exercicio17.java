//Luan Zelinski Corrêa - INFO 1 - Noturno
//
//Ler 80 números e ao final informar quantos números
//estão no intervalo entre 10 e 150

import java.util.Scanner;

public class Exercicio17 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int sim=0, nao=0;

        for(int i=1; i<=80; i++){
            System.out.print("Digite o "+i+"º número: ");
            int numero = input.nextInt();
            System.out.println();

            if(numero >= 10 && numero <= 150){
                sim += 1;
            }else{
                nao +=1;
            }
        }

        System.out.println(sim+" números no intervalo de 10 até 150");
        System.out.println("e "+nao+" fora deste intervalo");
    }
}
