//Luan Zelinski Corrêa - INFO 1 - Noturno
//
//Faça um programa em Java que receba a idade de 75 pessoas
//e mostre mensagem informando "maior de idade" e "menor de
//idade" para cada pessoa. Considere a idade a partir de 18
//anos como maior de idade.

import java.util.Scanner;

public class Exercicio18 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        for(int i=1; i<=75; i++){
            System.out.print("Digite a idade da pessoa de nº"+i+": ");
            float idade = input.nextInt();
            if(idade >= 18){
                System.out.println("Pessoa nº"+i+": maior de idade");
                System.out.println();
            }else{
                System.out.println("Pessoa nº"+i+": menor de idade");
                System.out.println();
            }
        }
    }
}
