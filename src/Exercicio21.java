//Luan Zelinski Corrêa - INFO 1 - Noturno
//
//Escrever um programa em Java que leia os dados de "N"
//pessoas (nome, sexo, idade e saúde) e informe se está
//apta ou não para cumprir o serviço militar obrigatório.
//
//Informe os totais.

import java.util.Scanner;

public class Exercicio21{
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        boolean situacao = true;

        String nome = null;

        int masapto=0, femapto=0, masnapto=0, femnapto=0;

        while(situacao == true){
            System.out.print("Digite o nome do cidadão: ");
            nome = input.next();

            System.out.print("Digite o sexo do cidadão (M/F): ");
            String sexo = input.next();

            System.out.print("Digite a idade do cidadão: ");
            int idade = input.nextInt();

            System.out.print("Digite S para saudável ou N para não saudável: ");
            String saude = input.next();

            if(idade >= 18 && saude.equals("S")){
                System.out.println("Cidadão apto a servir");
                System.out.println("");
                
                if(sexo.equals("M")){
                    masapto += 1;
                }else if(sexo.equals("F")){
                    femapto += 1;
                }
            }else{
                System.out.println("Cidadão não apto a servir");
                System.out.println("");

                if(sexo.equals("M")){
                    masnapto += 1;
                }else if(sexo.equals("F")){
                    femnapto += 1;
                }
            }

            System.out.print("Deseja continuar? (S/N) ");
            String continuar = input.next();
            System.out.println();

            if(continuar.equals("S")){
                situacao = true;
            }else{
                situacao = false;

                System.out.println("Mulheres aptas a servir: " + femapto);
                System.out.println("Mulheres inaptas a servir: " + femnapto);
                System.out.println("Homens aptos a servir: " + masapto);
                System.out.println("Homens inaptos a servir: " + masnapto);
            }

        }
    }
}