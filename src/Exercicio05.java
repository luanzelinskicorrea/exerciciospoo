//Luan Zelinski Corrêa - INFO 1 - Noturno
//
//Escrever um programa em Java que leia o nome de um aluno e
//as notas das três provas que ele obteve no semestre. No final
//informar o nome do aluno e a sua média (Média aritmética).

import java.util.Scanner;
public class Exercicio05 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Digite a primeira nota: ");
        float notaum = input.nextFloat();
        System.out.println();

        System.out.print("Digite a segunda nota: ");
        float notadois = input.nextFloat();
        System.out.println();

        System.out.print("Digite a terceira nota: ");
        float notatres = input.nextFloat();
        System.out.println();

        System.out.println("As médias são "+notaum+", "+notadois+" e "+notatres+". Média aritmética: "+((notaum+notadois+notatres)/3));
    }
}
