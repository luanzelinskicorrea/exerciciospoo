//Luan Zelinski Corrêa - INFO 1 - Noturno
//
//Escrever um programa em Java que leia o nome e sexo
//de 56 pessoas e informe o nome e se ela é homem ou
//mulher. No final informe o total de homens e mulheres.

import java.util.Scanner;

public class Exercicio19 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int feminino=0, masculino=0;

        for(int i=1; i<=56; i++){
            System.out.print("Digite o nome da pessoa: ");
            String nome = input.next();
            System.out.print("Digite o sexo da pessoa (M/F): ");
            String sexo = input.next();
            System.out.println();

            if(sexo.equals("M")){
                masculino += 1;
            }else if(sexo.equals("F")){
                feminino +=1;
            }
        }

        System.out.println(masculino+" homens e "+feminino+" mulheres");
    }
}
