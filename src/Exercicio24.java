//Luan Zelinski Corrêa - INFO 1 - Noturno
//
//Faça um programa em Java que receba "N" números
//e mostre positivo, negativo ou zero para cada
//número.

import java.util.Scanner;

public class Exercicio24 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        boolean situacao = true;

        while(situacao == true){
            System.out.print("Digite um número qualquer para verificarmos: ");
            float numero = input.nextFloat();

            if(numero > 0){
                System.out.println("Número positivo");
                System.out.println("");
            }else if(numero == 0){
                System.out.println("Número igual a zero");
                System.out.println("");
            }else if(numero < 0){
                System.out.println("Número negativo");
                System.out.println("");
            }

            System.out.print("Deseja continuar (S/N)? ");
            String continuar = input.next();
            System.out.println("");

            if(continuar.equals("S")){
                situacao = true;
            }else if(continuar.equals("N")){
                situacao = false;
            }
        }
    }
}
