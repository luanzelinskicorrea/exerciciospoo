//Luan Zelinski Corrêa - INFO 1 - Noturno
//
//Faça um programa em java que receba o preço de
//de custo e o preço de venda de 40 produtos.
//
//Mostre como resultado se houve lucro, prejuízo
//ou empate para cada produto.
//
//Informe média de preço de custo e de venda.

import java.util.Scanner;

public class Exercicio22 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        float mediacusto = 0, mediavenda = 0;

        for(int i=1; i<=40; i++){
            System.out.print("Digite o preço de custo do " + i + "º produto: R$ ");
            float custprod = input.nextFloat();
            mediacusto += custprod;

            System.out.print("Digite o preço de venda do " + i + "º produto: R$ ");
            float vendprod = input.nextFloat();
            mediavenda += vendprod;

            if((vendprod - custprod) > 0){
                System.out.println("Venda fechada com lucro");
            }else if((vendprod - custprod) == 0){
                System.out.println("Venda fechada com empate");
            }else{
                System.out.println("Venda fechada com prejuízo");
            }

            System.out.printf("Média de preço de custo R$ %.2f", (mediacusto/40));
            System.out.println("");
            System.out.printf("Média de preço de venda R$ %.2f", (mediavenda/40));
            System.out.println("\n");
        }
    }
}