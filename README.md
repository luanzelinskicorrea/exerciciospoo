# POO-M1



## Breve informação / Brief information

[PT-BR]
Repositório voltado a armazenamento de exercícios feitos em classe.
Projetos sem o prévio intuito de lucro ou lecionação.
Apenas projetos em Java armazenados neste repositório.

[EN-US]
Repository aimed at storing class projects.
Projects without the prior intention of profit or teaching.
Only Java projects stored in this repository.


## Softwares e plataformas usados / Used softwares and platforms

[PT-BR]


- [ ] Visual Studio Code / Intellij IDEA Community Edition - Para edição de códigos em Java.

- [ ] Git - Para controle de versões dos projetos.

- [ ] GitLab - Para gerenciar repositórios.

[EN-US]


- [ ] Visual Studio Code / Intellij IDEA Community Edition - For editing code in Java.

- [ ] Git - For project version control.

- [ ] GitLab - For managing repositories.


## Sobre a visibilidade / About the visibility

[PT-BR]
Mudanças de visibilidade (privado ou público) podem ser aleatórias.

[EN-US]
Visibility changes (private or public) can be random.


## Sobre a clonagem / About cloning

[PT-BR]
Clone a vontade, use a vontade, MAS NUNCA use com fins lucrativos/comerciais.

[EN-US]
Clone at will, use at will, BUT NEVER use for profit/commercial purposes.
