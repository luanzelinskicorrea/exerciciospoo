package aula5.servicos;
import aula5.bancos.Banco;

public class TransferenciaDeDinheiro {
    public static void transferir(Banco de, Banco para, Double valor){
        if(!ValidadorConta.transferenciaParaMesmaConta(de, para)){
            de.setSaldoBancario(de.getSaldoBancario() - valor);
            para.setSaldoBancario(para.getSaldoBancario() + valor);
        }
    }
}
