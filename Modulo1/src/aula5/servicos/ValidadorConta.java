package aula5.servicos;

import aula5.bancos.Banco;
import aula5.bancos.StatusConta;

public  class ValidadorConta {
    public static Boolean statusPermiteTransferencia(Banco banco){
        return banco.getStatusPermiteTransferencia().contains(banco.getStatusConta());
    }

    public static Boolean temDinheiroNaConta(Banco banco, Double valorTransferencia){

        if(banco.isPermiteChequeEspecial()){
            return valorTransferencia <= (banco.getSaldoBancario() + banco.getChequeEspecial());
        }else{
            return valorTransferencia <= banco.getSaldoBancario();
        }
    }

    public static Boolean valorMaiorQueZero(Double valorTransferencia){
        return valorTransferencia > 0;
    }

    public static boolean transferenciaParaMesmaConta(Banco de, Banco Para){
        return de.getConta() == Para.getConta() && de.getAgencia() == Para.getAgencia() && de.getNome() == Para.getNome();
    }
}
