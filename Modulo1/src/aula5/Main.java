package aula5;

import aula5.bancos.Banco;
import aula5.bancos.BancoDoBrasil;
import aula5.bancos.Itau;
import aula5.bancos.StatusConta;
import aula5.servicos.TransferenciaDeDinheiro;
import aula5.servicos.ValidadorConta;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        BancoDoBrasil bancoDoBrasil = new BancoDoBrasil();
        Itau itau = new Itau();

        bancoDoBrasil.setAgencia("0001");
        bancoDoBrasil.setConta("12345-0");
        bancoDoBrasil.setStatusConta(StatusConta.ATIVA);
        bancoDoBrasil.setSaldoBancario(500.00);
        bancoDoBrasil.setChequeEspecial(1000.00);

        itau.setAgencia("0001");
        itau.setConta("12345-0");
        itau.setStatusConta(StatusConta.ATIVA);
        itau.setSaldoBancario(00.00);
        itau.setChequeEspecial(3000.00);

        List<Banco> bancos = List.of(itau, bancoDoBrasil);

        Double valorTransferencia = 1500.00;
        if(ValidadorConta.statusPermiteTransferencia(itau) &&
           ValidadorConta.statusPermiteTransferencia(bancoDoBrasil) &&
           ValidadorConta.temDinheiroNaConta(itau, valorTransferencia) &&
           ValidadorConta.valorMaiorQueZero(valorTransferencia)){

            System.out.println("Pode transferir");
            TransferenciaDeDinheiro.transferir(itau, bancoDoBrasil, valorTransferencia);
        }else{
            System.out.println("Não pode transerir");
        }

        for(Banco banco: bancos){
            System.out.println("Nome do banco: " + banco.getNome());
            System.out.println("Agência da conta: " + banco.getAgencia());
            System.out.println("Número da conta: " + banco.getConta());
            System.out.println("Status da conta: " + banco.getStatusConta());

            System.out.println("O saldo da conta é: " + banco.getSaldoBancario());
        }
    }
}
