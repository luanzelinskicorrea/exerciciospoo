package aula5.bancos;

public enum StatusConta {
    ATIVA,
    BLOQUEADA,
    CANCELADA
}
