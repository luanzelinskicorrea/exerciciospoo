package aula5.bancos;

import java.util.List;

public abstract class Banco {

    private boolean permiteChequeEspecial = false;
    private String agencia;

    private String nome;
    private String conta;
    private StatusConta statusConta;

    public boolean isPermiteChequeEspecial(){
        return permiteChequeEspecial;
    }

    public Double getSaldoBancario() {
        return saldoBancario;
    }

    public void setSaldoBancario(Double saldoBancario) {
        this.saldoBancario = saldoBancario;
    }

    private Double saldoBancario = 0.0;

    public Double getChequeEspecial() {
        return chequeEspecial;
    }

    public void setChequeEspecial(Double chequeEspecial) {
        this.chequeEspecial = chequeEspecial;
    }

    private Double chequeEspecial = 0.00;

    public Banco(String nome){
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public String getConta() {
        return conta;
    }

    public void setConta(String conta) {
        this.conta = conta;
    }

    public StatusConta getStatusConta() {
        return statusConta;
    }

    public void setStatusConta(StatusConta statusConta) {
        this.statusConta = statusConta;
    }

    public abstract List<StatusConta> getStatusPermiteTransferencia();
}
