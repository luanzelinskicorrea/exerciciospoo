package aula5.bancos;

import java.util.List;

public class BancoDoBrasil extends Banco{
    public BancoDoBrasil(){
        super("Banco do Brasil");
    }

    @Override
    public List<StatusConta> getStatusPermiteTransferencia() {
        return List.of(StatusConta.ATIVA);
    }
}
