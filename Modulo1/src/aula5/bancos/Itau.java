package aula5.bancos;

import java.util.List;

public class Itau extends Banco{
    public Itau(){
        super("Itau");
    }

    @Override
    public List<StatusConta> getStatusPermiteTransferencia() {
        return List.of(StatusConta.BLOQUEADA);
    }
}
