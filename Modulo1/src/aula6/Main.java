package aula6;

public class Main {
    public static void main(String[] args) {
        Farmacia farmacia = new Farmacia("Minha Farmácia");

        Medicamento medicamento1 = new Medicamento("Paracetamol", 10.0);
        Medicamento medicamento2 = new Medicamento("Aspirina", 15.0);
        farmacia.cadastrarMedicamento(medicamento1);
        farmacia.cadastrarMedicamento(medicamento2);

        Funcionario gerente = new Funcionario("João", 2000.0);
        Funcionario atendente = new Funcionario("Maria", 1500.0);
        farmacia.cadastrarFuncionario(gerente);
        farmacia.cadastrarFuncionario(atendente);

        farmacia.setStatus(StatusFarmacia.FECHADO);

        System.out.println(farmacia);
        System.out.println("Pode vender medicamento: " + farmacia.podeVenderMedicamento());
    }
}

