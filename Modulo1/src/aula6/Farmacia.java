package aula6;

import java.util.ArrayList;
import java.util.List;

enum StatusFarmacia {
    ABERTO,
    FECHADO,
    FECHANDO
}

public class Farmacia {
    private String nome;
    private List<Medicamento> medicamentos;
    private List<Funcionario> funcionarios;
    private StatusFarmacia status;

    public Farmacia(String nome) {
        this.nome = nome;
        this.medicamentos = new ArrayList<>();
        this.funcionarios = new ArrayList<>();
        this.status = StatusFarmacia.ABERTO;
    }

    // Getters e Setters

    public void cadastrarMedicamento(Medicamento medicamento) {
        medicamentos.add(medicamento);
    }

    public void cadastrarFuncionario(Funcionario funcionario) {
        funcionarios.add(funcionario);
    }

    public boolean podeVenderMedicamento() {
        return status == StatusFarmacia.ABERTO || status == StatusFarmacia.FECHANDO;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Farmácia: ").append(nome).append("\n");
        sb.append("Status: ").append(status).append("\n");
        sb.append("Medicamentos:\n");
        for (Medicamento medicamento : medicamentos) {
            sb.append("- ").append(medicamento.getNome()).append(" (Valor: ").append(medicamento.getValor()).append(")\n");
        }
        sb.append("Funcionários:\n");
        for (Funcionario funcionario : funcionarios) {
            sb.append("- ").append(funcionario.getNome()).append(" (Salário: ").append(funcionario.getSalario()).append(")\n");
        }
        return sb.toString();
    }

    public StatusFarmacia getStatus() {
        return status;
    }

    public void setStatus(StatusFarmacia status) {
        this.status = status;
    }
}
