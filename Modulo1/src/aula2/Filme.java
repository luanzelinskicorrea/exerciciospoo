package aula2;

public class Filme extends Programa{
    private String duracao;

    public Filme(String nome, String ano, String duracao) {
        super(nome, ano);
        this.duracao = duracao;
    }

    public String getDuracao() {
        return duracao;
    }

    @Override
    public String getNome() {
        return "O nome do filme é: "+super.getNome();
    }

    @Override
    public String getAno() {
        return "O ano do filme é: "+super.getAno();
    }



    public void setDuracao(String duracao) {
        this.duracao = duracao;
    }

    @Override
    public String getTempo() {
        return "A duração do filme é de: "+this.getDuracao();
    }
}
