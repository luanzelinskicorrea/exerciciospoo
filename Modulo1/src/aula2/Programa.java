package aula2;

public abstract class Programa {
    private String nome;
    private String ano;

    public Programa(String nome, String ano) {
        this.nome = nome;
        this.ano = ano;
    }

    public abstract String getTempo();


    public String getNome() {
        return nome;
    }

    public String getAno() {
        return ano;
    }
}
