package aula2;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        Serie serie = new Serie("Coração Marcado","2022","2 Temporadas");
        Filme filme = new Filme("Velozese Furiosos 10","2023","201 Minutos");

        System.out.println();

        List<Programa> programas = List.of(serie, filme);

        for(Programa programa: programas){
            System.out.println(programa.getNome());
            System.out.println(programa.getAno());
            System.out.println(programa.getTempo());
            System.out.println("");
        }
    }
}
