package aula2;

public class Serie extends Programa{
    private String temporadas;

    public Serie(String nome, String ano, String temporadas) {
        super(nome, ano);
        this.temporadas = temporadas;
    }

    public String getTemporadas() {
        return temporadas;
    }

    public void setTemporadas(String temporadas) {
        this.temporadas = temporadas;
    }

    @Override
    public String getNome() {
        return "O nome da série é: "+super.getNome();
    }

    @Override
    public String getAno() {
        return "O ano da série é: "+super.getAno();
    }

    @Override
    public String getTempo() {
        return "A quantidade de temporadas é: "+this.getTemporadas();
    }
}