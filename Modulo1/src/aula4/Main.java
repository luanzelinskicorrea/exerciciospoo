package aula4;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        Gerente gerente = new Gerente();
        gerente.setNome("João da Silva");
        gerente.setSalario(1000);

        Atendente atendente = new Atendente();
        atendente.setNome("José Medeiros");
        atendente.setSalario(1000);

        List<Funcionario> funcionarios = List.of(gerente, atendente);
        for (Funcionario funcionario: funcionarios){
            System.out.println("O nome do funcionário é: "+ funcionario.getNome());
            System.out.println("O salário do funcionário é: " + funcionario.getSalario()+"\n");
        }
    }
}
