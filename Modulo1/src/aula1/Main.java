package aula1;

public class Main {
    public static void main(String[] args) {
        Cachorro cachorro = new Cachorro();
        Gato gato = new Gato();

        cachorro.setNome("Luke");
        cachorro.setIdade(2);
        cachorro.setRaca("Spitz Alemão");

        gato.setNome("Thor");
        gato.setIdade(2);
        gato.setRaca("Persa");

        System.out.println("Nome do cachorro: " + cachorro.getNome());
        System.out.println("Idade do cachorro: " + cachorro.getIdade());
        System.out.println("Raça do cachorro: " + cachorro.getRaca());
        System.out.println("Tipo do animal: " + cachorro.getTipo());

        System.out.println("Nome do gato: " + gato.getNome());
        System.out.println("Idade do gato: " + gato.getIdade());
        System.out.println("Raça do gato: " + gato.getRaca());
        System.out.println("Tipo do animal: " + gato.getTipo());
    }
}